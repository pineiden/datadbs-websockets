from setuptools import setup, find_packages

setup(name='data_ws',
      version='0.0.1',
      description='DataDBS for Websockets',
      url='http://www.gitlab.com/dpineda/data_ws',
      author='David Pineda Osorio',
      author_email='dpineda@csn.uchile.cl',
      license='MIT',
      packages=['datadbs'],
      install_requires=find_packages(),
      package_dir={'datad_ws': 'data_ws'},
      package_data={
          'data_ws': ['../doc', '../docs', '../requeriments.txt']},
      zip_safe=False)
